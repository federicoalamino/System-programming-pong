# Trabajo realizado en FCEN-UBA(CS. de la Computación) 

## Enunciado
Aquí se encontrar el enunciado dado por la catedra sobre el planteo del ejercicio.

## Informe
Sección donde se puede ver el informe realizado para dicho trabajo practico, en formato PDF realizado en LaTeX.

## SRC
Aquí se encontraran todos los archivos necesarios para el funcionamiento del pong.
La versión realizada se probo su correctitud utilizando BOCHS.

## Finalidad y aprendizaje:
El aprendizaje que obtuve realizando este trabajo practico fue el funcionamiento interno de una PC a bajo nivel. 
 
1. Descriptores de segmentos.  
2. Interrupciones. IDT
3. Flags. 
4. GDT
5. PD, PDEntrys 
6. Syscalls. 
7. TSS