#include "debug.h"
#include "screen.h"
#include "sched.h"
#include "i386.h"

static struct debug crash_state;
bool debug_mode = FALSE;
bool debug_screen = FALSE;

void debug_save_crash_with_error(char* reason, uint32_t edi, uint32_t esi,
	uint32_t ebp, uint32_t temp, uint32_t ebx, uint32_t edx, uint32_t ecx,
	uint32_t eax, uint32_t err, uint32_t eip, uint32_t cs, uint32_t eflags,
	uint32_t esp, uint32_t ss) {

	if (!debug_mode)
		return;

	crash_state.reason = reason;

	crash_state.eax = eax;
	crash_state.ebx = ebx;
	crash_state.ecx = ecx;
	crash_state.edx = edx;
	crash_state.esi = esi;
	crash_state.edi = edi;
	crash_state.ebp = ebp;
	crash_state.esp = esp;
	crash_state.eip = eip;

	crash_state.cs = cs;
	crash_state.ds = reg_ds();
	crash_state.es = reg_es();
	crash_state.fs = reg_fs();
	crash_state.gs = reg_gs();
	crash_state.ss = ss;

	crash_state.eflags = eflags;

	crash_state.cr0 = rcr0();
	crash_state.cr2 = rcr2();
	crash_state.cr3 = rcr3();
	crash_state.cr4 = rcr4();

	crash_state.tr = rtr();

	crash_state.err = err;
	crash_state.has_err = 1;
}

void debug_save_crash(char* reason, uint32_t edi, uint32_t esi, uint32_t ebp,
	uint32_t temp, uint32_t ebx, uint32_t edx, uint32_t ecx, uint32_t eax,
	uint32_t eip, uint32_t cs, uint32_t eflags, uint32_t esp,
	uint32_t ss) {

	if (!debug_mode)
		return;

	crash_state.reason = reason;

	crash_state.eax = eax;
	crash_state.ebx = ebx;
	crash_state.ecx = ecx;
	crash_state.edx = edx;
	crash_state.esi = esi;
	crash_state.edi = edi;
	crash_state.ebp = ebp;
	crash_state.esp = esp;
	crash_state.eip = eip;

	crash_state.cs = cs;
	crash_state.ds = reg_ds();
	crash_state.es = reg_es();
	crash_state.fs = reg_fs();
	crash_state.gs = reg_gs();
	crash_state.ss = ss;

	crash_state.eflags = eflags;

	crash_state.cr0 = rcr0();
	crash_state.cr2 = rcr2();
	crash_state.cr3 = rcr3();
	crash_state.cr4 = rcr4();

	crash_state.tr = rtr();

	crash_state.has_err = 0;

	debug_draw();
}

#define DEBUG_HEIGHT	41
#define DEBUG_WIDTH	36
#define DEBUG_X		22
#define DEBUG_Y		0
#define VALUE_COLOR	(C_FG_WHITE | C_BG_BROWN)
#define NAME_COLOR	(C_FG_BLACK | C_BG_BROWN)
#define TITLE_COLOR	(C_FG_WHITE | C_BG_BLACK)

#define Pelem(R, W) { \
	print(#R, 24 + (elem % 2) * 20, (elem / 2) * 2, NAME_COLOR); \
	print_hex(crash_state.R, W, 28 + (elem % 2) * 20, (elem / 2) * 2, \
			VALUE_COLOR); \
	elem++; \
}
#define P8(R) Pelem(R,8)
#define P4(R) Pelem(R,4)
#define SEP() elem = elem + elem % 2
#define TITLE(s) { print(s, 24, (elem / 2) * 2, TITLE_COLOR); elem+=2; }

void debug_draw() {
	int elem = 4;
	screen_simpleBox(DEBUG_Y, DEBUG_X, DEBUG_HEIGHT, DEBUG_WIDTH, C_FG_BROWN);
	print("DEBUG", 35, 0, C_FG_WHITE | C_BG_BROWN);
	print(crash_state.reason, 23, 2, TITLE_COLOR);

	TITLE("                       REGISTROS")
	P8(eax);	P8(ebx);
	P8(ecx);	P8(edx);
	P8(esi);	P8(edi);
	P8(ebp);	P8(esp);
	P8(eip);
	SEP();
	TITLE("                       SEGMENTOS")
	P4(cs);		P4(ds);
	P4(es);		P4(fs);
	P4(gs);		P4(ss);
	P8(tr)
	SEP();
	TITLE("            REGISTROS DE CONTROL")
	P8(cr0);	P8(cr2);
	P8(cr3);	P8(cr4);
	SEP();
	P8(eflags);
	if(crash_state.has_err)
		P8(err);
}

void set_debug_mode(bool value) {
	debug_mode = value;
}

void set_debug_screen(bool value) {
	debug_screen = value;
}

bool get_debug_mode() {
	return debug_mode;
}

bool get_debug_screen() {
	return debug_screen;
}
