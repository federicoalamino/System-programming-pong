#ifndef __DEBUG_H__
#define __DEBUG_H__

#include "stdint.h"
#include "defines.h"

struct debug {
	char* reason;
	uint32_t eax, ebx, ecx, edx, esi, edi, ebp, esp, eip,
		 cs, ds, es, fs, gs, ss,
		 eflags,
		 cr0, cr2, cr3, cr4,
		 tr,
		 err, has_err;
};

extern bool debug_mode;
extern bool debug_screen;

void debug_save_crash_with_error(char* reason, uint32_t edi, uint32_t esi,
	uint32_t ebp, uint32_t temp, uint32_t ebx, uint32_t edx, uint32_t ecx,
	uint32_t eax, uint32_t err, uint32_t eip, uint32_t cs, uint32_t eflags,
	uint32_t esp, uint32_t ss);
void debug_save_crash(char* reason, uint32_t edi, uint32_t esi, uint32_t ebp,
	uint32_t temp, uint32_t ebx, uint32_t edx, uint32_t ecx, uint32_t eax,
	uint32_t eip, uint32_t cs, uint32_t eflags, uint32_t esp, uint32_t ss);
void debug_draw();

void set_debug_mode(bool value);
void set_debug_screen(bool value);
bool get_debug_mode();
bool get_debug_screen();

#endif
