/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================
  definicion de estructuras para administrar tareas
*/

#include "tss.h"

tss tss_initial;
tss tss_idle;

void tss_init() {
	init_tss_initial();
	init_tss_idle();
	
	// Inicializo tareas
	for(int i = 0; i < 6; i++) {
		// Defino base del descriptor TASK correspondiente
		gdt_entry* desc_task = &gdt[GDT_IDX_TSS_TASK_BASE_DESC + i];
		tss* tss_task = &tss_tasks[i];
		desc_task->base_0_15 = (uint32_t) tss_task;
		desc_task->base_23_16 = (uint32_t) tss_task>>16;
		desc_task->base_31_24 = (uint32_t) tss_task>>24;

		// Creo un cr3 para cada tarea disponible.
		cr3_tasks[i] = mmu_initTaskDir(i);

		// Creo un stack nivel 0 para cada tarea disponible.
		esp0_tasks[i] = mmu_nextFreeKernelPage();
	}

	// Inicializo handlers
	for(int i = 0; i < 6; i++) {
		// Defino base del descriptor HANDLER correspondiente
		gdt_entry* desc_handler = &gdt[GDT_IDX_TSS_HANDLER_BASE_DESC + i];
		tss* tss_handler = &tss_handlers[i];
		desc_handler->base_0_15 = (uint32_t) tss_handler;
		desc_handler->base_23_16 = (uint32_t) tss_handler>>16;
		desc_handler->base_31_24 = (uint32_t) tss_handler>>24;
	}
}

void init_tss_initial() {
	// Defino base del descriptor INITIAL
	gdt_entry* desc_inicial = &gdt[GDT_IDX_TSS_INICIAL_DESC];
	desc_inicial->base_0_15 = (uint32_t) &tss_initial;
	desc_inicial->base_23_16 = (uint32_t) &tss_initial>>16;
	desc_inicial->base_31_24 = (uint32_t) &tss_initial>>24;
}

void init_tss_idle() {
	// Defino contexto inicial de la tarea IDLE
	set_tss_default(tss_idle);
	tss_idle.eip = 0x1C000;
	tss_idle.cr3 = KERNEL_PAGE_DIR;
	tss_idle.esp =  0x2b000;
	tss_idle.ebp =	 0x2b000;
	tss_idle.eflags = 0x00000202;
	tss_idle.cs = GDT_IDX_CODE_LVL0_DESC << 3;
	tss_idle.ds = GDT_IDX_DATA_LVL0_DESC << 3;
	tss_idle.es = GDT_IDX_DATA_LVL0_DESC << 3;
	tss_idle.fs = GDT_IDX_DATA_LVL0_DESC << 3;
	tss_idle.gs = GDT_IDX_DATA_LVL0_DESC << 3;
	tss_idle.ss = GDT_IDX_DATA_LVL0_DESC << 3;

	// Defino base del descriptor IDLE
	gdt_entry* desc_idle = &gdt[GDT_IDX_TSS_IDLE_DESC];
	desc_idle->base_0_15 = (uint32_t) &tss_idle;
	desc_idle->base_23_16 = (uint32_t) &tss_idle>>16;
	desc_idle->base_31_24 = (uint32_t) &tss_idle>>24;
}

void init_tss_task(int task_index, uint8_t task_type) {
	tss* tss_task = &tss_tasks[task_index];

	set_tss_default(tss_task);

	tss_task->eip = TASK_CODE_1;
	// tss_task->cr3 = cr3_tasks[task_index];
	tss_task->cr3 = mmu_initTaskDir(task_type);
	tss_task->esp =  TASK_STACK;
	tss_task->ebp =	TASK_STACK;
	
	tss_task->esp0 = esp0_tasks[task_index] + 1024 * 2;
	tss_task->ss0 = (GDT_IDX_DATA_LVL0_DESC << 3);

	tss_task->cs =  (GDT_IDX_CODE_LVL3_DESC << 3) + 3;
	tss_task->ds =  (GDT_IDX_DATA_LVL3_DESC << 3) + 3;
	tss_task->es  = (GDT_IDX_DATA_LVL3_DESC << 3) + 3;
	tss_task->ss  = (GDT_IDX_DATA_LVL3_DESC << 3) + 3;
	tss_task->fs  = (GDT_IDX_DATA_LVL3_DESC << 3) + 3;
	tss_task->gs  = (GDT_IDX_DATA_LVL3_DESC << 3) + 3;

	tss_task->eflags = INITIAL_EFLAGS;
}

void tss_register_handler(int handler_index, f_handler_t* func) {
	dir_handlers[handler_index] = (uint32_t)func;
}

void init_tss_handler(int handler_index) {
	// Obtengo la instancia de la tarea asociada (es el mismo index del handler).
	tss* tss_task = &tss_tasks[handler_index];
	// Obtengo la instancia del handler
	tss* tss_handler = &tss_handlers[handler_index];

	set_tss_default(tss_handler);

	tss_handler->eip = dir_handlers[handler_index];
	
	tss_handler->cr3 = tss_task->cr3;
	tss_handler->esp =  TASK_HANDLER_STACK;
	tss_handler->ebp =	TASK_HANDLER_STACK;

	tss_handler->esp0 = tss_task->esp0 + 1024 * 2;
	tss_handler->ss0 = (GDT_IDX_DATA_LVL0_DESC << 3);

	tss_handler->cs =  (GDT_IDX_CODE_LVL3_DESC << 3) + 3;
	tss_handler->ds =  (GDT_IDX_DATA_LVL3_DESC << 3) + 3;
	tss_handler->es  = (GDT_IDX_DATA_LVL3_DESC << 3) + 3;
	tss_handler->ss  = (GDT_IDX_DATA_LVL3_DESC << 3) + 3;
	tss_handler->ds  = (GDT_IDX_DATA_LVL3_DESC << 3) + 3;
	tss_handler->fs  = (GDT_IDX_DATA_LVL3_DESC << 3) + 3;
	tss_handler->gs  = (GDT_IDX_DATA_LVL3_DESC << 3) + 3;

	tss_handler->eflags = INITIAL_EFLAGS;
}

void set_tss_default(tss* tss) {
	tss->ptl = 0;
	tss->unused0 = 0;
	tss->esp0 = 0;
	tss->ss0 = 0;
	tss->unused1 = 0;
	tss->esp1 = 0;
	tss->ss1 = 0;
	tss->unused2 = 0;
	tss->esp2 = 0;
	tss->ss2 = 0;
	tss->unused3 = 0;
	tss->cr3 = 0;
	tss->eip = 0;
	tss->eflags = 0;
	tss->eax = 0;
	tss->ecx = 0;
	tss->edx = 0;
	tss->ebx = 0;
	tss->esp = 0;
	tss->ebp = 0;
	tss->esi = 0;
	tss->edi = 0;
	tss->es = 0;
	tss->unused4 = 0;
	tss->cs = 0;
	tss->unused5 = 0;
	tss->ss = 0;
	tss->unused6 = 0;
	tss->ds = 0;
	tss->unused7 = 0;
	tss->fs = 0;
	tss->unused8 = 0;
	tss->gs = 0;
	tss->unused9 = 0;
	tss->ldt = 0;
	tss->unused10 = 0;
	tss->dtrap = 0;
	tss->iomap = 0;
}

