/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================
  definicion de funciones del scheduler
*/

#ifndef __SCHED_H__
#define __SCHED_H__

#include "stdint.h"
#include "screen.h"
#include "tss.h"
#include "game.h"

/* Saltar a la tarea idle, donde 0xA0 es el selector en la TSS */
static inline void sched_idle() {
	asm("ljmp $0xA0, $0x0");
}

typedef void (*f_handler_t)();

void sched_init();

int16_t sched_nextTask();

int get_current_task();
bool is_handler_enabled(int index);
bool is_task_enabled(int index);
void kill_task();
uint16_t get_current_task_selector();
void sched_register_handler(f_handler_t* func);

void set_debug_mode(bool value);
void set_debug_screen(bool value);
bool get_debug_mode();
bool get_debug_screen();

#endif	/* !__SCHED_H__ */
