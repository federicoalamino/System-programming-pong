/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================
  definicion de la tabla de descriptores globales
*/

#include "gdt.h"

#define TASK_ENTRY_LVL_0 \
    ((gdt_entry) { \
        .limit_0_15 = TSS_TASK_LIMIT, \
        .type = 0x9, \
        .dpl = 0x0, \
        .p = 1 \
    })

#define TASK_ENTRY_LVL_3 \
    ((gdt_entry) { \
        .limit_0_15 = TSS_TASK_LIMIT, \
        .type = 0x9, \
        .dpl = 0x3, \
        .p = 1 \
    })

gdt_entry gdt[GDT_COUNT] = {
    /* Descriptor nulo*/
    /* Offset = 0x00 */
    [GDT_IDX_NULL_DESC] = (gdt_entry) {
        (uint16_t)    0x0000,         /* limit[0:15]  */
        (uint16_t)    0x0000,         /* base[0:15]   */
        (uint8_t)     0x00,           /* base[23:16]  */
        (uint8_t)     0x00,           /* type         */
        (uint8_t)     0x00,           /* s            */
        (uint8_t)     0x00,           /* dpl          */
        (uint8_t)     0x00,           /* p            */
        (uint8_t)     0x00,           /* limit[16:19] */
        (uint8_t)     0x00,           /* avl          */
        (uint8_t)     0x00,           /* l            */
        (uint8_t)     0x00,           /* db           */
        (uint8_t)     0x00,           /* g            */
        (uint8_t)     0x00,           /* base[31:24]  */
    },

    [GDT_IDX_CODE_LVL0_DESC] = (gdt_entry) {
        (uint16_t)    0xffff,         /* limit[0:15]  */
        (uint16_t)    0x0000,         /* base[0:15]   */
        (uint8_t)     0x00,           /* base[23:16]  */
        (uint8_t)     0x08,           /* type         */
        (uint8_t)     0x01,           /* s            */
        (uint8_t)     0x00,           /* dpl          */
        (uint8_t)     0x01,           /* p            */
        (uint8_t)     0x00,           /* limit[16:19] */
        (uint8_t)     0x00,           /* avl          */
        (uint8_t)     0x00,           /* l            */
        (uint8_t)     0x01,           /* db           */
        (uint8_t)     0x01,           /* g            */
        (uint8_t)     0x00,           /* base[31:24]  */
    },

    [GDT_IDX_DATA_LVL0_DESC] = (gdt_entry) {
        (uint16_t)    0xffff,         /* limit[0:15]  */
        (uint16_t)    0x0000,         /* base[0:15]   */
        (uint8_t)     0x00,           /* base[23:16]  */
        (uint8_t)     0x02,           /* type         */
        (uint8_t)     0x01,           /* s            */
        (uint8_t)     0x00,           /* dpl          */
        (uint8_t)     0x01,           /* p            */
        (uint8_t)     0x00,           /* limit[16:19] */
        (uint8_t)     0x00,           /* avl          */
        (uint8_t)     0x00,           /* l            */
        (uint8_t)     0x01,           /* db           */
        (uint8_t)     0x01,           /* g            */
        (uint8_t)     0x00,           /* base[31:24]  */
    },

    [GDT_IDX_CODE_LVL3_DESC] = (gdt_entry) {
        (uint16_t)    0xffff,         /* limit[0:15]  */
        (uint16_t)    0x0000,         /* base[0:15]   */
        (uint8_t)     0x00,           /* base[23:16]  */
        (uint8_t)     0x08,           /* type         */
        (uint8_t)     0x01,           /* s            */
        (uint8_t)     0x03,           /* dpl          */
        (uint8_t)     0x01,           /* p            */
        (uint8_t)     0x00,           /* limit[16:19] */
        (uint8_t)     0x00,           /* avl          */
        (uint8_t)     0x00,           /* l            */
        (uint8_t)     0x01,           /* db           */
        (uint8_t)     0x01,           /* g            */
        (uint8_t)     0x00,           /* base[31:24]  */
    },    
   
   [GDT_IDX_DATA_LVL3_DESC] = (gdt_entry) {
        (uint16_t)    0xffff,         /* limit[0:15]  */
        (uint16_t)    0x0000,         /* base[0:15]   */
        (uint8_t)     0x00,           /* base[23:16]  */
        (uint8_t)     0x02,           /* type         */
        (uint8_t)     0x01,           /* s            */
        (uint8_t)     0x03,           /* dpl          */
        (uint8_t)     0x01,           /* p            */
        (uint8_t)     0x00,           /* limit[16:19] */
        (uint8_t)     0x00,           /* avl          */
        (uint8_t)     0x00,           /* l            */
        (uint8_t)     0x01,           /* db           */
        (uint8_t)     0x01,           /* g            */
        (uint8_t)     0x00,           /* base[31:24]  */
    },

  [GDT_IDX_VIDEO_DESC] = (gdt_entry) {
       (uint16_t)    0x27ff,         /* limit[0:15]  */
       (uint16_t)    0x8000,         /* base[0:15]   */
       (uint8_t)     0x0B,           /* base[23:16]  */
       (uint8_t)     0x02,           /* type         */
       (uint8_t)     0x01,           /* s            */
       (uint8_t)     0x00,           /* dpl          */
       (uint8_t)     0x01,           /* p            */
       (uint8_t)     0x00,           /* limit[16:19] */
       (uint8_t)     0x00,           /* avl          */
       (uint8_t)     0x00,           /* l            */
       (uint8_t)     0x01,           /* db           */
       (uint8_t)     0x00,           /* g            */
       (uint8_t)     0x00,           /* base[31:24]  */
    },

    [GDT_IDX_TSS_IDLE_DESC] = TASK_ENTRY_LVL_0,
    [GDT_IDX_TSS_INICIAL_DESC] = TASK_ENTRY_LVL_0,

    [GDT_IDX_TSS_TASK_BASE_DESC + 0] = TASK_ENTRY_LVL_3,
    [GDT_IDX_TSS_TASK_BASE_DESC + 1] = TASK_ENTRY_LVL_3,
    [GDT_IDX_TSS_TASK_BASE_DESC + 2] = TASK_ENTRY_LVL_3,
    [GDT_IDX_TSS_TASK_BASE_DESC + 3] = TASK_ENTRY_LVL_3,
    [GDT_IDX_TSS_TASK_BASE_DESC + 4] = TASK_ENTRY_LVL_3,
    [GDT_IDX_TSS_TASK_BASE_DESC + 5] = TASK_ENTRY_LVL_3,
    
    [GDT_IDX_TSS_HANDLER_BASE_DESC + 0] = TASK_ENTRY_LVL_3,
    [GDT_IDX_TSS_HANDLER_BASE_DESC + 1] = TASK_ENTRY_LVL_3,
    [GDT_IDX_TSS_HANDLER_BASE_DESC + 2] = TASK_ENTRY_LVL_3,
    [GDT_IDX_TSS_HANDLER_BASE_DESC + 3] = TASK_ENTRY_LVL_3,
    [GDT_IDX_TSS_HANDLER_BASE_DESC + 4] = TASK_ENTRY_LVL_3,
    [GDT_IDX_TSS_HANDLER_BASE_DESC + 5] = TASK_ENTRY_LVL_3
};

gdt_descriptor GDT_DESC = {
    sizeof(gdt) - 1,
    (uint32_t) &gdt
};
