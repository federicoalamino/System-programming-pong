/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================
*/

#include "game.h"
#include "sched.h"
#include "mmu.h"
#include "tss.h"
// #include "debug.h"

void game_init() {
	// Inicializo jugadores
	game_init_player(PLAYER_1);
	game_init_player(PLAYER_2);

	players[PLAYER_1].fg_color = C_FG_LIGHT_BLUE;
	players[PLAYER_1].bg_color = C_BG_LIGHT_BLUE;
	
	players[PLAYER_2].fg_color = C_FG_LIGHT_RED;
	players[PLAYER_2].bg_color = C_BG_LIGHT_RED;

	// Inicializo la pantalla
	game_draw();
}


// Inicializo valores de jugadores
void game_init_player(int player) {
	players[player].available_balls = DEFAULT_AVAILABLE_BALLS;
	players[player].cursor.y = DEFAULT_CURSOR_Y;
	for(int i = 0; i < 3; i++)
		players[player].balls[i].with_message = FALSE;
}

// Pinta la pantalla del juego
void game_draw() {
	game_draw_map();
	game_draw_cursors();
	game_draw_balls(PLAYER_1);
	game_draw_balls(PLAYER_2);
	game_draw_panel();
}

// Pinta el mapa
void game_draw_map()  {
	screen_simpleBox(0, 1, MAP_ROWS, 78, C_FG_LIGHT_GREY);
}

// Pinta los cursores de los jugadores
void game_draw_cursors() {
	/***** Jugador 1 *****/
	screen_simpleBox(0, 0, 40, 1, C_FG_WHITE);
	screen_simpleBox(players[PLAYER_1].cursor.y - 3, 0, 7, 1, players[PLAYER_1].fg_color);

	/***** Jugador 2 *****/
	screen_simpleBox(0, 79, 40, 1, C_FG_WHITE);
	screen_simpleBox(players[PLAYER_2].cursor.y - 3, 79, 7, 1, players[PLAYER_2].fg_color);
}

// Pinta las pelotas del jugador pasado por parámetro
void game_draw_balls(int player) {
	for (int i = 0; i < 3; i++) {
		if (players[player].balls[i].enabled) {
			uint8_t x = players[player].balls[i].x;
			uint8_t y = players[player].balls[i].y;
			int color = players[player].fg_color;
			if (player == PLAYER_1)
				print("O", x, y, color | C_BG_LIGHT_GREY);
			else
				print("O", 79-x, y, color | C_BG_LIGHT_GREY);
		}
	}
}

// Pinta el tablero de los jugadores
void game_draw_panel() {
	/***** Jugador 1 *****/
	// Tablero
	screen_simpleBox(41, 1, 8, 39, players[PLAYER_1].fg_color);
	// Puntos
	print("Puntos: ", 2, 42, C_FG_BLACK | C_BG_WHITE);
	print_dec(players[PLAYER_1].points, 4, 10, 42, C_FG_BLACK | C_BG_WHITE);
	// Pelotas disponibles
	print("Pelotas:  ", 2, 43, C_FG_BLACK | C_BG_WHITE);
	print_dec(players[PLAYER_1].available_balls, 2, 12, 43, C_FG_BLACK | C_BG_WHITE);
	// Pelotas en juego
	for (int i = 0; i < 3; i++) {
		if (players[PLAYER_1].balls[i].enabled) 
			print("O", 22 + i * 2, 42, C_FG_BLACK | C_BG_WHITE);
		else
			print("X", 22 + i * 2, 42, C_FG_BLACK | C_BG_WHITE);
	}
	// Mensaje de las pelotas
	for (int i = 0; i < 3; i++) {
		if (players[PLAYER_1].balls[i].with_message) {
			print("Pelota ", 2, 45 + i, C_FG_BLACK | C_BG_WHITE);
			print_dec(i, 1, 9, 45 + i, C_FG_BLACK | C_BG_WHITE);
			print(": ", 10, 45 + i, C_FG_BLACK | C_BG_WHITE);
			print((const char*)players[PLAYER_1].balls[i].message, 12, 45 + i, C_FG_BLACK | C_BG_WHITE);
		} else {
			screen_simpleBox(45 + i, 2, 1, 20, players[PLAYER_1].fg_color);
		}
	}

	// ***** Jugador 2 *****
	// Tablero
	screen_simpleBox(41, 40, 8, 39, players[PLAYER_2].fg_color);
	// Puntos 
	print("Puntos: ", 41, 42, C_FG_BLACK | C_BG_WHITE);
	print_dec(players[PLAYER_2].points, 4, 49, 42, C_FG_BLACK | C_BG_WHITE);
	// Pelotas disponibles
	print("Pelotas:  ", 41, 43, C_FG_BLACK | C_BG_WHITE);
	print_dec(players[PLAYER_2].available_balls, 2, 51, 43, C_FG_BLACK | C_BG_WHITE);
	// Pelotas en juego
	for (int i = 0; i < 3; i++) {
		if (players[PLAYER_2].balls[i].enabled) 
			print("O", 61 + i * 2, 42, C_FG_BLACK | C_BG_WHITE);
		else
			print("X", 61 + i * 2, 42, C_FG_BLACK | C_BG_WHITE);
	}
	// Mensaje de las pelotas
	for (int i = 0; i < 3; i++) {
		if (players[PLAYER_2].balls[i].with_message) {
			print("Pelota ", 41, 45 + i, C_FG_BLACK | C_BG_WHITE);
			print_dec(i, 1, 48, 45 + i, C_FG_BLACK | C_BG_WHITE);
			print(": ", 49, 45 + i, C_FG_BLACK | C_BG_WHITE);
			print((const char*)players[PLAYER_2].balls[i].message, 51, 45 + i, C_FG_BLACK | C_BG_WHITE);
		} else {
			screen_simpleBox(45 + i, 41, 1, 20, players[PLAYER_2].fg_color);
		}
	}

	if (get_debug_mode() == TRUE)
		print("DebugMode:ON ", 60, 49, C_FG_WHITE | C_BG_BLACK);
	else
		print("DebugMode:OFF", 60, 49, C_FG_WHITE | C_BG_BLACK);
}

void game_update() {
	game_draw_map();

	for (int player = 0; player < 2; player++) {
		int player_enemy = (player == PLAYER_1) ? PLAYER_2 : PLAYER_1;

		for (int i = 0; i < 3; i++) {
			if (players[player].balls[i].enabled) {
				// Movimiento horizontal
				players[player].balls[i].x += players[player].balls[i].direction_x;
				// Movimiento vertical
				players[player].balls[i].y += players[player].balls[i].action * players[player].balls[i].direction_y;

				// Si la bola llegó al borde superior o inferior rebota
				if (players[player].balls[i].y == 0 || players[player].balls[i].y == 39)
					players[player].balls[i].direction_y *= Inverted;

				// Si la bola llegó al otro lado pero está el cursor del otro player rebota
				if (players[player].balls[i].x == 78) {
					int y = players[player].balls[i].y;
					int enemy_cursor_y = players[player_enemy].cursor.y;
					if (y >= enemy_cursor_y - 3 && y <= enemy_cursor_y + 3)
						players[player].balls[i].direction_x = Inverted;
				}

				// Si la bola volvió pero está el cursor del player rebota
				if (players[player].balls[i].x == 1) {
					int y = players[player].balls[i].y;
					int cursor_y = players[player].cursor.y;
					if (y >= cursor_y - 3 && y <= cursor_y + 3)
						players[player].balls[i].direction_x = Normal;
				}

				// Si la bola llegó al otro lado es punto para el player!
				if (players[player].balls[i].x == 79) {
					players[player].points++;
					// players[player].available_balls++;
					game_kill_ball(player, i);
				}

				// Si la bola volvió es punto para el otro player.
				if (players[player].balls[i].x == 0) {
					game_kill_ball(player, i);
					players[player_enemy].points++;
				}
			}
		}

		game_draw_balls(player);
	}

	game_draw_panel();

	if (players[PLAYER_1].points >= WINNING_POINTS) {
		game_drawvictory(PLAYER_1);
		set_debug_mode(TRUE);
		set_debug_screen(TRUE);
		return;
	}
	if (players[PLAYER_2].points >= WINNING_POINTS) {
		game_drawvictory(PLAYER_2);
		set_debug_mode(TRUE);
		set_debug_screen(TRUE);
		return;
	}
}

// Devuelve true o false si la pelota ball_index % 3 está habilitada
bool game_ball_enabled(int ball_index) {
	if (ball_index < 3)
		return players[PLAYER_1].balls[ball_index].enabled;
	else
		return players[PLAYER_2].balls[ball_index%3].enabled;
}

void game_on_key_press(uint8_t key_pressed) {
	if (get_debug_screen() == TRUE && key_pressed != DBG_TOGGLE)
		return;

	switch (key_pressed) {
		case DBG_TOGGLE:
			if (get_debug_screen() == TRUE) {
				set_debug_screen(FALSE);
			} else {
				if (get_debug_mode() == TRUE)
					set_debug_mode(FALSE);
				else
					set_debug_mode(TRUE);
			}
			break;

		case PLAYER_1_UP:
			if (players[PLAYER_1].cursor.y - 3 > 0)
				players[PLAYER_1].cursor.y--;
			break;
		case PLAYER_1_DOWN:
			if (players[PLAYER_1].cursor.y + 3 < MAP_ROWS-1)
				players[PLAYER_1].cursor.y++;
			break;
		case PLAYER_1_NEW_BALL_1:
			new_ball(PLAYER_1, 0);
			break;
		case PLAYER_1_NEW_BALL_2:
			new_ball(PLAYER_1, 1);
			break;
		case PLAYER_1_NEW_BALL_3:
			new_ball(PLAYER_1, 2);
			break;

		case PLAYER_2_UP:
			if (players[PLAYER_2].cursor.y - 3 > 0)
				players[PLAYER_2].cursor.y--;
			break;
		case PLAYER_2_DOWN:
			if (players[PLAYER_2].cursor.y + 3 < MAP_ROWS-1)
				players[PLAYER_2].cursor.y++;
			break;
		case PLAYER_2_NEW_BALL_1:
			new_ball(PLAYER_2, 3);
			break;
		case PLAYER_2_NEW_BALL_2:
			new_ball(PLAYER_2, 4);
			break;
		case PLAYER_2_NEW_BALL_3:
			new_ball(PLAYER_2, 5);
			break;
	}
}

void new_ball(int player, uint8_t type) {
	if (players[player].available_balls > 0) {
		int ball_index = get_free_ball_index(player);
		
		if (ball_index != -1) {
			init_tss_task(ball_index + player * 3, type);
			players[player].available_balls--;
			players[player].balls[ball_index].type = type;
			players[player].balls[ball_index].x = 1;
			players[player].balls[ball_index].y = players[player].cursor.y;
			players[player].balls[ball_index].direction_x = Normal;
			players[player].balls[ball_index].direction_y = Normal;
			players[player].balls[ball_index].action = Center;
			players[player].balls[ball_index].enabled = 1;
			players[player].balls[ball_index].with_message = FALSE;
			players[player].balls[ball_index].with_handler = 0;
		}
	}
}

int get_free_ball_index(int player) {
	int index = -1;
	for (uint8_t i = 0; i < 3; i++)
		if (players[player].balls[i].enabled == FALSE) {
			return i;
		}

	return index;
}

void game_kill_ball(int player, int index) {
	players[player].balls[index].enabled = 0;
	players[player].balls[index].with_handler = 0;
	players[player].balls[index].with_message = FALSE;
}

void set_ball_action(e_action_t action) {
	int index = get_current_task();

	int player = PLAYER_1;
	if (index >= 3) {
		index = index % 3;
		player = PLAYER_2;
	}

	players[player].balls[index].action = action;
}

uint8_t get_current_ball_x() {
	int index = get_current_task();

	int player = PLAYER_1;
	if (index >= 3) {
		index = index % 3;
		player = PLAYER_2;
	}

	return players[player].balls[index].x;
}

uint8_t get_current_ball_y() {
	int index = get_current_task();

	int player = PLAYER_1;
	if (index >= 3) {
		index = index % 3;
		player = PLAYER_2;
	}

	return players[player].balls[index].y;
}

void set_ball_message(char * msg) {
	int index = get_current_task();

	int player = PLAYER_1;
	if (index >= 3) {
		index = index % 3;
		player = PLAYER_2;
	}

	for(uint8_t i = 0; i < 20; i++)
		players[player].balls[index].message[i] = msg[i];
	
	players[player].balls[index].with_message = TRUE;
}

// Handler de print_debug_info, quita el parámetro err para las excepciones que lo tienen pusheado en la pila.
void print_debug_info_with_errorCode(uint32_t pila1, uint32_t pila2, uint32_t pila3, uint32_t pila4, uint32_t pila5, int reason, uint32_t edi, uint32_t esi,
	uint32_t ebp, uint32_t temp, uint32_t ebx, uint32_t edx, uint32_t ecx,
	uint32_t eax, uint32_t err, uint32_t eip, uint32_t cs, uint32_t eflags,
	uint32_t esp, uint32_t ss) {
	print_debug_info(pila1, pila2, pila3, pila4, pila5, reason, edi, esi,
	ebp, temp, ebx, edx, ecx, eax, eip, cs, eflags,	esp, ss);
}

void print_debug_info(uint32_t pila1, uint32_t pila2, uint32_t pila3, uint32_t pila4, uint32_t pila5, int reason, uint32_t edi, uint32_t esi,
	uint32_t ebp, uint32_t temp, uint32_t ebx, uint32_t edx, uint32_t ecx,
	uint32_t eax, uint32_t eip, uint32_t cs, uint32_t eflags,
	uint32_t esp, uint32_t ss) {
	if (get_debug_mode() == TRUE) {
		set_debug_screen(TRUE);
		screen_simpleBox(0, 20, 40, 40, C_BG_BLACK);
		screen_simpleBox(1, 21, 39, 38, C_FG_LIGHT_GREY);
		screen_simpleBox(1, 21, 1, 38, C_FG_RED);
		screen_simpleBox(2, 20, 1, 40, C_BG_BLACK);
		
		print(get_exception_desc(reason), 21, 1, C_FG_WHITE | C_BG_RED);
		print("eax",22, 6, C_FG_BLACK | C_BG_LIGHT_GREY);
		print_hex(eax,8, 26, 6, C_FG_WHITE | C_BG_LIGHT_GREY);
		print("ebx",22, 8, C_FG_BLACK | C_BG_LIGHT_GREY);
		print_hex(ebx,8, 26, 8, C_FG_WHITE | C_BG_LIGHT_GREY);
		print("ecx",22, 10, C_FG_BLACK | C_BG_LIGHT_GREY);
		print_hex(ecx,8, 26, 10, C_FG_WHITE | C_BG_LIGHT_GREY);
		print("edx",22, 12, C_FG_BLACK | C_BG_LIGHT_GREY);
		print_hex(edx,8, 26, 12, C_FG_WHITE | C_BG_LIGHT_GREY);
		print("esi",22, 14, C_FG_BLACK | C_BG_LIGHT_GREY);
		print_hex(esi,8, 26, 14, C_FG_WHITE | C_BG_LIGHT_GREY);
		print("edi",22, 16, C_FG_BLACK | C_BG_LIGHT_GREY);
		print_hex(edi,8, 26, 16, C_FG_WHITE | C_BG_LIGHT_GREY);
		print("ebp",22, 18, C_FG_BLACK | C_BG_LIGHT_GREY);
		print_hex(ebp,8, 26, 18, C_FG_WHITE | C_BG_LIGHT_GREY);
		print("esp",22, 20, C_FG_BLACK | C_BG_LIGHT_GREY);
		print_hex(esp,8, 26, 20, C_FG_WHITE | C_BG_LIGHT_GREY);
		print("eip",22, 22, C_FG_BLACK | C_BG_LIGHT_GREY);
		print_hex(eip,8, 26, 22, C_FG_WHITE | C_BG_LIGHT_GREY);
		print("cs",23,24, C_FG_BLACK | C_BG_LIGHT_GREY);
		print_hex(cs,4, 26, 24, C_FG_WHITE | C_BG_LIGHT_GREY);
		print("ds",23, 26, C_FG_BLACK | C_BG_LIGHT_GREY);
		print_hex(rds(),4, 26, 26, C_FG_WHITE | C_BG_LIGHT_GREY);
		print("es",23, 28, C_FG_BLACK | C_BG_LIGHT_GREY);
		print_hex(res(),4, 26, 28, C_FG_WHITE | C_BG_LIGHT_GREY);
		print("fs",23, 30, C_FG_BLACK | C_BG_LIGHT_GREY);
		print_hex(rfs(),4, 26, 30, C_FG_WHITE | C_BG_LIGHT_GREY);
		print("gs",23, 32, C_FG_BLACK | C_BG_LIGHT_GREY);
		print_hex(rgs(),4, 26, 32, C_FG_WHITE | C_BG_LIGHT_GREY);
		print("ss",23, 34, C_FG_BLACK | C_BG_LIGHT_GREY);
		print_hex(ss,4, 26, 34, C_FG_WHITE | C_BG_LIGHT_GREY);

		print("eflags",23, 37, C_FG_BLACK | C_BG_LIGHT_GREY);
		print_hex(eflags,8, 30, 37, C_FG_WHITE | C_BG_LIGHT_GREY);

		print("cr0",37, 6, C_FG_BLACK | C_BG_LIGHT_GREY);
		print_hex(rcr0(),8, 41, 6, C_FG_WHITE | C_BG_LIGHT_GREY);
		print("cr2",37, 8, C_FG_BLACK | C_BG_LIGHT_GREY);
		print_hex(rcr2(),8, 41, 8, C_FG_WHITE | C_BG_LIGHT_GREY);
		print("cr3",37, 10, C_FG_BLACK | C_BG_LIGHT_GREY);
		print_hex(rcr3(),8, 41, 10, C_FG_WHITE | C_BG_LIGHT_GREY);
		print("cr4",37, 12, C_FG_BLACK | C_BG_LIGHT_GREY);
		print_hex(rcr4(),8, 41, 12, C_FG_WHITE | C_BG_LIGHT_GREY);

		print("STACK",37, 28, C_FG_BLACK | C_BG_LIGHT_GREY);
		print_hex(pila5,8, 37, 30, C_FG_WHITE | C_BG_LIGHT_GREY);
		print_hex(pila4,8, 37, 31, C_FG_WHITE | C_BG_LIGHT_GREY);		
		print_hex(pila3,8, 37, 32, C_FG_WHITE | C_BG_LIGHT_GREY);
		print_hex(pila2,8, 37, 33, C_FG_WHITE | C_BG_LIGHT_GREY);
		print_hex(pila1,8, 37, 34, C_FG_WHITE | C_BG_LIGHT_GREY);
	}
}

const char* get_exception_desc(int reason) {
	const char* ret;
	switch(reason) {
		case 0: ret = "0 - Divide Error Exception        "; break;
		case 1: ret = "1 - Debug Exception               "; break;
		case 2: ret = "2 - NMI Interrupt                 "; break;
		case 3: ret = "3 - Breakpoint                    "; break;
		case 4: ret = "4 - Overflow Exception            "; break;
		case 5: ret = "5 - BOUND Range Exceeded Exception"; break;
		case 6: ret = "6 - Invalid Opcode Exception      "; break;
		case 7: ret = "7 - Device Not Available Exception"; break;
		case 8: ret = "8 - Double Fault Exception        "; break;
		case 9: ret = "9 - Coprocessor Segment Overrun   "; break;
		case 10: ret = "10 - Invalid TSS Exception        "; break;
		case 11: ret = "11 - Segment Not Present          "; break;
		case 12: ret = "12 - Stack Fault Exception        "; break;
		case 13: ret = "13 - General Protection Exception "; break;
		case 14: ret = "14 - Page-Fault Exception         "; break;
		case 15: ret = "15 - Reserved                     "; break;
		case 16: ret = "16 - x87 FPU Floating-Point Error "; break;
		case 17: ret = "17 - Alignment Check Exception    "; break;
		case 18: ret = "18 - Machine-Check Exception      "; break;
		case 19: ret = "19 - SIMD Floating-Point Exception"; break;
		case 20: ret = "20 - Virtualization Exception     "; break;
	}
	return ret;
}

void game_drawvictory(int player) {
	screen_simpleBox(3, 21, 12, 38, players[player].bg_color);
	screen_simpleBox(5, 23, 8, 34, C_FG_BLACK);
	print("#     # # #   # #   # ##### ### ", 24, 6,  players[player].fg_color | C_BG_BLACK);
	print("#     #   ##  # ##  # #     #  #", 24, 7,  players[player].fg_color | C_BG_BLACK);
	print("#     # # # # # # # # ####  #  #", 24, 8,  players[player].fg_color | C_BG_BLACK);
	print("#  #  # # # # # # # # #     ### ", 24, 9,  players[player].fg_color | C_BG_BLACK);
	print("#  #  # # #  ## #  ## #     # # ", 24, 10, players[player].fg_color | C_BG_BLACK);
	print(" ## ##  # #   # #   # ##### #  #", 24, 11, players[player].fg_color | C_BG_BLACK);
}