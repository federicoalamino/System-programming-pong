/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================

    Definiciones globales del sistema.
*/

#ifndef __DEFINES_H__
#define __DEFINES_H__

/* Bool */
/* -------------------------------------------------------------------------- */
#define TRUE                    0x00000001
#define FALSE                   0x00000000
#define ERROR                   1
#define NULL  	                0
typedef int bool;

/* Atributos paginas */
/* -------------------------------------------------------------------------- */
#define PAG_P                   0x00000001
#define PAG_R                   0x00000000
#define PAG_RW                  0x00000002
#define PAG_S                   0x00000000
#define PAG_US                  0x00000004

/* Misc */
#define TSS_TASK_LIMIT 0x0067
#define PAGE_SIZE 4096
#define INITIAL_EFLAGS 0x00000202
/* -------------------------------------------------------------------------- */

/* Indices en la gdt */
/* -------------------------------------------------------------------------- */
#define GDT_COUNT 40

#define GDT_IDX_NULL_DESC     	   		0
#define GDT_IDX_CODE_LVL0_DESC		    14
#define GDT_IDX_DATA_LVL0_DESC	    	15
#define GDT_IDX_CODE_LVL3_DESC	    	16
#define GDT_IDX_DATA_LVL3_DESC	    	17
#define GDT_IDX_VIDEO_DESC		    	18
#define GDT_IDX_TSS_INICIAL_DESC		19 	
#define GDT_IDX_TSS_IDLE_DESC			20
#define GDT_IDX_TSS_TASK_BASE_DESC		21
#define GDT_IDX_TSS_HANDLER_BASE_DESC	27


/* Offsets en la gdt */
/* -------------------------------------------------------------------------- */
#define GDT_OFF_NULL_DESC           (GDT_IDX_NULL_DESC << 3)

/* Selectores de segmentos */
/* -------------------------------------------------------------------------- */

/* Direcciones de memoria */
/* -------------------------------------------------------------------------- */
#define BOOTSECTOR               0x00001000 /* direccion fisica de comienzo del bootsector (copiado) */
#define KERNEL                   0x00001200 /* direccion fisica de comienzo del kernel */
#define VIDEO                    0x000B8000 /* direccion fisica del buffer de video */

/* Direcciones virtuales de código, pila y datos */
/* -------------------------------------------------------------------------- */
#define TASK_CODE_1         0x08000000 /* direccion virtual del codigo */
#define TASK_CODE_2         0x08001000 /* direccion virtual del codigo */
#define TASK_STACK 			0x08001c00 /* direccion virtual de la pila de tarea */
#define TASK_HANDLER_STACK	0x08002000 /* direccion virtual de la pila de handler */

/* Direcciones fisicas de codigos */
/* -------------------------------------------------------------------------- */
/* En estas direcciones estan los códigos de todas las tareas. De aqui se
 * copiaran al destino indicado por TASK_<i>_CODE_ADDR.
 */
#define TASK_0_CODE_ADDR 0x10000
#define TASK_1_CODE_ADDR 0x12000
#define TASK_2_CODE_ADDR 0x14000
#define TASK_3_CODE_ADDR 0x16000
#define TASK_4_CODE_ADDR 0x18000
#define TASK_5_CODE_ADDR 0x1A000

/* Direcciones fisicas de directorios y tablas de paginas del KERNEL */
/* -------------------------------------------------------------------------- */
#define KERNEL_PAGE_DIR          0x0002b000
#define KERNEL_PAGE_TABLE_0      0x0002c000
#define INICIO_PAGINAS_LIBRES_KERNEL 0x100000
#define INICIO_PAGINAS_LIBRES_TASK 0x400000

#endif  /* !__DEFINES_H__ */
