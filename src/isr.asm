	; ** por compatibilidad se omiten tildes **
; ==============================================================================
; TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
; ==============================================================================
; definicion de rutinas de atencion de interrupciones

%include "print.mac"

%define IS_TALK 0x80000001
%define IS_WHERE 0x80000002
%define IS_SET_HANDLER 0x80000003
%define IS_INFORM_ACTION 0x8000ffff
%define IDLE_DESC 20

BITS 32

section .data
sched_task_offset:     dd 0xFFFFFFFF
sched_task_selector:   dw 0xFFFF

msg: db 'Se produjo un error de tipo: ', 0
largo EQU $-msg

;; PIC
extern pic_finish1

;; Sched
extern sched_nextTask

;; Game
extern game_on_key_press

extern kill_task
extern sched_register_handler
extern get_current_task
extern get_current_task_selector
extern set_ball_action
extern get_current_ball_x
extern get_current_ball_y
extern set_ball_message
extern print_debug_info
extern print_debug_info_with_errorCode
;;
;; Definición de MACROS
;; -------------------------------------------------------------------------- ;;

%macro ISR 1
global _isr%1

_isr%1:
	pushad
	; No va mas, fue parte del ejercicio 2
    ; mov eax, %1
    ; add eax, 48
	; mov edi, 0
	; mov ebx, msg
	; mov ecx, largo
	; print_text_pm ebx, ecx, 0x07, 0, 0
	; print_text_pm eax, 2, 0x07, 30, 30

	; Pusheo el código de excepción
	push %1
	; Pusheo valores del stack de la tarea que se rompió
	mov eax, [esp + 4*12]
	mov ebx, [eax]
	push ebx
	mov ebx, [eax + 4]
	push ebx
	mov ebx, [eax + 8]
	push ebx
	mov ebx, [eax + 12]
	push ebx
	mov ebx, [eax + 16]
	push ebx
	call print_debug_info
	; Mato la tarea actual
	jmp kill_task
	
	popad
	iret
%endmacro

%macro ISR_WITH_ERROR 1
global _isr%1

_isr%1:
	pushad

	; Pusheo el código de excepción
	push %1
	; Pusheo valores del stack de la tarea que se rompió
	mov eax, [esp + 4*13]
	mov ebx, [eax]
	push ebx
	mov ebx, [eax + 4]
	push ebx
	mov ebx, [eax + 8]
	push ebx
	mov ebx, [eax + 12]
	push ebx
	mov ebx, [eax + 16]
	push ebx
	call print_debug_info_with_errorCode
	; Mato la tarea actual
	jmp kill_task
	
	popad
	iret
%endmacro


;; Rutina de atención de las EXCEPCIONES
;; -------------------------------------------------------------------------- ;;
ISR 0
ISR 1
ISR 2
ISR 3
ISR 4
ISR 5
ISR 6
ISR 7
ISR_WITH_ERROR 8
ISR 9
ISR_WITH_ERROR 10
ISR_WITH_ERROR 11
ISR_WITH_ERROR 12
ISR_WITH_ERROR 13
ISR_WITH_ERROR 14
ISR 15
ISR 16
ISR_WITH_ERROR 17
ISR 18
ISR 19

;; Rutina de atención del RELOJ
global _isr32
_isr32:
	pushad
	call nextClock
	call pic_finish1

	call sched_nextTask

	str cx
	cmp ax, cx
	je ._fin

	mov [sched_task_selector], ax
	jmp far [sched_task_offset]

	._fin:
		popad
		iret
;; -------------------------------------------------------------------------- ;;

;; Rutina de atención del TECLADO
global _isr33
_isr33:
	pushad
	
	in al, 0x60
	push WORD ax
	call game_on_key_press
	add esp, 2

	._end:
		call pic_finish1
		popad
	iret
;; -------------------------------------------------------------------------- ;;

;; Rutinas de atención de las SYSCALLS
global _isr47
_isr47:
	;push eax(esp+28), ecx(esp+24), edx(esp+20), ebx(esp+16), esp(esp+12), ebp(esp+8), esi(esp+4), edi (<-esp)
	pushad
	
	cmp eax, IS_TALK
	je .talk
	cmp eax, IS_WHERE
	je .where
	cmp eax, IS_SET_HANDLER
	je .set_handler
	cmp eax, IS_INFORM_ACTION
	je .inform_action
	jmp .kill_task

	.talk:
		push ebx
		call set_ball_message
		add esp, 4
		jmp .fin	

	.where:
		call get_current_ball_x
		mov [esp + 16], eax ; ebx
		call get_current_ball_y
		mov [esp + 24], eax ; ecx
		jmp .fin
		
	.set_handler:
		push ebx
		call sched_register_handler
		add esp, 4
		jmp .fin	

	.inform_action:
		push ebx
		call set_ball_action
		call get_current_task_selector
		mov [sched_task_selector], ax
		jmp far [sched_task_offset]
		jmp .fin

	.kill_task:
		call get_current_task
		push eax
		jmp kill_task
		add esp, 4
	.fin: 
		popad
		iret

;; -------------------------------------------------------------------------- ;;

;; Funciones Auxiliares
;; -------------------------------------------------------------------------- ;;
isrNumber:           dd 0x00000000
isrClock:            db '|/-\'
nextClock:
        pushad
        inc DWORD [isrNumber]
        mov ebx, [isrNumber]
        cmp ebx, 0x4
        jl .ok
                mov DWORD [isrNumber], 0x0
                mov ebx, 0
        .ok:
                add ebx, isrClock
                print_text_pm ebx, 1, 0x0f, 49, 79
                popad
        ret
