/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================
  definicion de funciones del manejador de memoria
*/

#include "mmu.h"

const uint32_t task_codes[] = {
	TASK_0_CODE_ADDR,
	TASK_1_CODE_ADDR,
	TASK_2_CODE_ADDR,
	TASK_3_CODE_ADDR,
	TASK_4_CODE_ADDR,
	TASK_5_CODE_ADDR
};

void mmu_init() {
 proxima_pagina_libre_kernel = INICIO_PAGINAS_LIBRES_KERNEL;
 proxima_pagina_libre_task = INICIO_PAGINAS_LIBRES_TASK;
}

uint32_t mmu_nextFreeKernelPage() {
    unsigned int pagina_libre = proxima_pagina_libre_kernel;
    proxima_pagina_libre_kernel += PAGE_SIZE;
    return pagina_libre;
}

uint32_t mmu_nextFreeTaskPage() {
    unsigned int pagina_libre = proxima_pagina_libre_task;
    proxima_pagina_libre_task += PAGE_SIZE;
    return pagina_libre;
}


void mmu_mapPage(uint32_t virtual, uint32_t cr3, uint32_t phy, uint32_t attrs) {
	pde* pd = (pde*)((cr3>>12)<<12);	
	
	uint32_t indice_pd = virtual>>22;
	uint32_t indice_pt = (virtual<<10)>>22;

	pde* pdEntry = pd;
	// pdEntry += indice_pd
	if(pdEntry[indice_pd].present == 0){
		pdEntry[indice_pd].present = 1;
		pdEntry[indice_pd].rw = 1;
		pdEntry[indice_pd].us = attrs;

		pte* pt = (pte*)(mmu_nextFreeKernelPage()>>12);
			// Inicializo page table
		for(int i = 0; i < 1024;i++)
			((pte*)(pt))[i] = (pte) {0};
		
		pdEntry[indice_pd].tabla = (uint32_t) pt;
	}	 

	pte* ptEntry =  (pte*) (pdEntry[indice_pd].tabla<<12);
	ptEntry[indice_pt].present = 1;
	ptEntry[indice_pt].rw = 1;
	ptEntry[indice_pt].us = attrs;
	ptEntry[indice_pt].base = phy>>12;
	
	tlbflush();
	return;
}

uint32_t mmu_unmapPage(uint32_t virtual, uint32_t cr3) {
	pde* pd = (pde*)((cr3>>12)<<12);
	
	uint32_t indice_pd = virtual>>22;
	uint32_t indice_pt = (virtual<<10)>>22;

	pte* ptEntry =  (pte*) (pd[indice_pd].tabla<<12);
	ptEntry[indice_pt].present = 0;

    return 0;
}

uint32_t mmu_initTaskDir(uint8_t task_type) {
	// Pido memoria para el directorio de páginas
	uint32_t dir_pd =  mmu_nextFreeKernelPage();

	// Seteo las entradas a 0
	for(int i = 0; i < 1024;i++)
		((pde*)(dir_pd))[i] = (pde) {0};

	// Mapeo los primeros 4MB del Kernel.
	for(int i = 0; i < 1024; i++)
		mmu_mapPage(i*PAGE_SIZE, dir_pd, i*PAGE_SIZE, 0);

	uint32_t dir_pagina1 = mmu_nextFreeTaskPage();
	uint32_t dir_pagina2 = mmu_nextFreeTaskPage();

	mmu_mapPage(TASK_CODE_1, dir_pd, dir_pagina1, 1);
	mmu_mapPage(TASK_CODE_2, dir_pd, dir_pagina2, 1);

	// Decido qué tarea copiar.
	uint32_t* dir_codigo = (uint32_t*) task_codes[task_type];

	// Mapeo temporalmente 0x8000000
	uint32_t cr3 = rcr3();
	mmu_mapPage(dir_pagina1, cr3, dir_pagina1, 1);
	mmu_mapPage(dir_pagina2, cr3, dir_pagina2, 1);

	// Copiamos el código de la tarea.
	uint32_t* dir_inicio = (uint32_t*) dir_pagina1;
	for(int i = 0; i < 2 * 1024; i++){
		dir_inicio[i] = dir_codigo[i];
	}

	// Desmapeo antes de salir
	mmu_unmapPage(dir_pagina1, cr3);
	mmu_unmapPage(dir_pagina2, cr3);
	
	return dir_pd;
}

uint32_t mmu_initKernelDir() {
	pde* dir = (pde*) KERNEL_PAGE_DIR;

	// Seteo todas las entradas en 0.
	for(int i = 0; i < 1024;i++)
		dir[i] = (pde) {0};
	
	//Inicializamos la primer entrada de dir
	dir[0].present = 1;
	dir[0].rw=1;
    dir[0].us=0;
    dir[0].pwt=0;
    dir[0].pcd=0;
    dir[0].a=0;
    dir[0].ignored=0;
    dir[0].ps=0;
    dir[0].g =0;
    dir[0].avl = 0;
    dir[0].tabla = (KERNEL_PAGE_TABLE_0>>12);
    
    //Inicializamos la tabla
    pte* tabla = (pte*) KERNEL_PAGE_TABLE_0;
    for(int i = 0; i < 1024;i++){
		tabla[i].present = 1;
		tabla[i].rw=1;
		tabla[i].us=0;
		tabla[i].pwt=0;
		tabla[i].pcd=0;
		tabla[i].a=0;
		tabla[i].d=0;
		tabla[i].pat=0;
		tabla[i].g =0;
		tabla[i].disp = 0;
		tabla[i].base = i;
		}    
    return 0;
}








