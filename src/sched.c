/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================
  definicion de funciones del scheduler
*/

#include "game.h"
#include "tss.h"
// #include "debug.h"

// El quantum 6 es siempre la idle
#define IDLE_TASK 6
bool debug_mode = FALSE;
bool debug_screen = FALSE;

// Tarea actual (quantum)
// Jugador A -> 0..2
// Jugador B -> 3..5
int current_task = 0;

uint8_t remainding_cursor_ticks = 24;

void sched_init() {
}

int get_current_task() {
	return current_task;
}

int16_t sched_nextTask() {
	remainding_cursor_ticks--;
	// Cada 24 ticks pinto los cursores.
	if (remainding_cursor_ticks == 0) {
		remainding_cursor_ticks = 24;
		game_draw_cursors();
	}
	
	current_task++;
	current_task = current_task % 7;

	// Si estoy mostrando detalles de debug ejecuto idle hasta salir del modo.
	if (debug_mode && debug_screen)
		return GDT_IDX_TSS_IDLE_DESC << 3;

	if (current_task != IDLE_TASK) {
		if (is_handler_enabled(current_task)) {
			init_tss_handler(current_task);
			return (GDT_IDX_TSS_HANDLER_BASE_DESC + current_task) << 3;
		} else {
			if (is_task_enabled(current_task)) {
				return (GDT_IDX_TSS_TASK_BASE_DESC + current_task) << 3;	
			}
		}
	} else {
		// Si estoy en el quantum 7 actualizo los datos del juego.
		game_update();
	}

  	return GDT_IDX_TSS_IDLE_DESC << 3;
}

bool is_handler_enabled(int index) {
	int player = PLAYER_1;
	if (index >= 3) {
		index = index % 3;
		player = PLAYER_2;
	}
	
	return (players[player].balls[index].enabled && players[player].balls[index].with_handler);
}

bool is_task_enabled(int index) {
	int player = PLAYER_1;
	if (index >= 3) {
		index = index % 3;
		player = PLAYER_2;
	}

	return players[player].balls[index].enabled;
}

void sched_register_handler(f_handler_t* func) {
	int index = get_current_task();
	tss_register_handler(index, func);

	int player = PLAYER_1;
	if (index >= 3) {
		index = index % 3;
		player = PLAYER_2;
	}

	players[player].balls[index].with_handler = 1;
}

void kill_task() {
	int index = get_current_task();
	int player = PLAYER_1;
	if (index >= 3) {
		index = index % 3;
		player = PLAYER_2;
	}

	players[player].balls[index].enabled = 0;
	players[player].balls[index].with_handler = 0;
	players[player].balls[index].with_message = FALSE;

	sched_idle();
}

uint16_t get_current_task_selector() {
	int current_task = get_current_task();
	return (GDT_IDX_TSS_TASK_BASE_DESC + current_task) << 3;
}

void set_debug_mode(bool value) {
	debug_mode = value;
}

void set_debug_screen(bool value) {
	debug_screen = value;
}

bool get_debug_mode() {
	return debug_mode;
}

bool get_debug_screen() {
	return debug_screen;
}