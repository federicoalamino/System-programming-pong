; ** por compatibilidad se omiten tildes **
; ==============================================================================
; TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
; ==============================================================================

%include "print.mac"
%define GDT_OFF_TSS_INICIAL_DESC 19<<3 
%define GDT_OFF_TSS_IDLE_DESC 20<<3
%define GDT_OFF_DATA_LVL0_DESC 15<<3
%define GDT_OFF_VIDEO_DESC 18<<3
%define KERNEL_STACK_DIR 0x2b000

global start

extern GDT_DESC
extern idt_init
extern IDT_DESC
extern pic_reset
extern pic_enable
extern mmu_init
extern mmu_initKernelDir
extern tss_init
extern sched_init
extern game_init

;; Saltear seccion de datos
jmp start

;;
;; Seccion de datos.
;; -------------------------------------------------------------------------- ;;
start_rm_msg db     'Iniciando kernel en Modo Real'
start_rm_len equ    $ - start_rm_msg

start_pm_msg db     'Iniciando kernel en Modo Protegido'
start_pm_len equ    $ - start_pm_msg

start_lu_msg db     '38/17;316/17;916/12'
start_lu_len equ    $ - start_lu_msg

;;
;; Seccion de código.
;; -------------------------------------------------------------------------- ;;

;; Punto de entrada del kernel.
;ORG 0x1200
BITS 16
start:
    ; Deshabilitar interrupciones
    cli

    ; Cambiar modo de video a 80 X 50
    ; ax = 1003h -> para poder tener 16 colores de background
    mov ax, 1003h
    int 10h ; set mode 03h
    xor bx, bx
    mov ax, 1112h
    int 10h ; load 8x8 font

    ; Imprimir mensaje de bienvenida
    print_text_rm start_rm_msg, start_rm_len, 0x07, 0, 0

    ; Habilitar A20
    call A20_enable
    
    ; Cargar la GDT
    lgdt [GDT_DESC]

    ; Setear el bit PE del registro CR0
    mov eax, cr0
    or eax, 1
    mov cr0, eax
    
    ; Saltar a modo protegido
    jmp 14<<3:modoprotegido
    
modoprotegido:
BITS 32
    ; Establecer selectores de segmentos
	mov ax, GDT_OFF_DATA_LVL0_DESC
	mov ds, ax
	mov es, ax
	mov ss, ax
	mov ax, GDT_OFF_VIDEO_DESC
	mov gs, ax
    
    ; Establecer la base de la pila
	mov ebp, KERNEL_STACK_DIR
	mov esp, KERNEL_STACK_DIR

    ; Imprimir mensaje de bienvenida
    print_text_pm start_pm_msg, start_pm_len, 0x07, 0, 0

    ; Inicializar pantalla
    call game_init
 
	; Inicializar el manejador de memoria
	call mmu_init

    ; Inicializar el directorio de paginas
	call mmu_initKernelDir
    
    ; Cargar directorio de paginas
	mov eax, 0x0002b000
    mov cr3, eax
    	
    ; Habilitar paginacion
    mov eax, cr0
    or eax, 1<<31
	mov cr0, eax
	
	; Imprimimos las libretas
	print_text_pm start_lu_msg, start_lu_len, 0x07, 30, 30
		
    ; Inicializar tss
    call tss_init;

    ; Inicializar el scheduler
    call sched_init

    ; Inicializar la IDT
	call idt_init

    ; Cargar IDT	
	lidt [IDT_DESC]
 
    ; Configurar controlador de interrupciones
	call pic_reset;
	call pic_enable;

    ; Cargar tarea inicial
    mov ax, GDT_OFF_TSS_INICIAL_DESC
    ltr ax

    ; Habilitar interrupciones
	sti

    ; Saltar a la primera tarea: Idle
    jmp GDT_OFF_TSS_IDLE_DESC:0

    ; Ciclar infinitamente (por si algo sale mal...)
    mov eax, 0xFFFF
    mov ebx, 0xFFFF
    mov ecx, 0xFFFF
    mov edx, 0xFFFF
    jmp $

;; -------------------------------------------------------------------------- ;;

%include "a20.asm"
