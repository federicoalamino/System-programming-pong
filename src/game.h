/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================
*/

#ifndef __GAME_H__
#define __GAME_H__

#include "stdint.h"
#include "defines.h"
#include "screen.h"
#include "mmu.h"
#include "sched.h"

// Defines
#define PLAYER_1 0
#define PLAYER_2 1

// Pelotas máximas de cada jugador
#define DEFAULT_AVAILABLE_BALLS 15
#define WINNING_POINTS 10
#define DEFAULT_CURSOR_Y 20

// Teclas
#define W_KEY		0x11
#define S_KEY		0x1f
#define Z_KEY		0x2c
#define X_KEY		0x2d
#define C_KEY		0x2e

#define I_KEY		0x17
#define K_KEY		0x25
#define B_KEY		0x30
#define N_KEY		0x31
#define M_KEY		0x32

#define Y_KEY		0x95

#define PLAYER_1_UP			W_KEY
#define PLAYER_1_DOWN		S_KEY
#define PLAYER_1_NEW_BALL_1	Z_KEY
#define PLAYER_1_NEW_BALL_2	X_KEY
#define PLAYER_1_NEW_BALL_3	C_KEY

#define PLAYER_2_UP			I_KEY
#define PLAYER_2_DOWN		K_KEY
#define PLAYER_2_NEW_BALL_1	B_KEY
#define PLAYER_2_NEW_BALL_2	N_KEY
#define PLAYER_2_NEW_BALL_3	M_KEY

#define DBG_TOGGLE Y_KEY

#define MAP_ROWS 40

// Tipos
typedef void (*f_handler_t)();

typedef enum e_action {
    Up = -1,
    Center = 0,
    Down = 1,
} e_action_t;

typedef enum e_direction {
    Normal = 1,
    Inverted = -1,
} e_direction_t;

typedef struct ball {
	uint8_t type;
	uint8_t x;
	uint8_t y;
	e_direction_t direction_x;
	e_direction_t direction_y;
	e_action_t action;
	bool enabled;
	char message[20];
	bool with_message;
	bool with_handler;
} ball;

typedef struct cursor {
	uint8_t y;
} cursor;

typedef struct player {
	uint8_t points;
	uint8_t available_balls;
	ball balls[3];
	cursor cursor;
	int fg_color;
	int bg_color;
} player;

void game_init_player();
void game_draw();
void game_draw_map();
void game_draw_cursors();
void game_draw_balls();
void game_draw_panel();
void game_update();
void new_ball(int player, uint8_t type);
int get_free_ball_index(int player);
void game_kill_ball(int player, int ball_index);
const char* get_exception_desc(int reason);
void print_debug_info(uint32_t pila1, uint32_t pila2, uint32_t pila3, uint32_t pila4, uint32_t pila5, int reason, uint32_t edi, uint32_t esi,
	uint32_t ebp, uint32_t temp, uint32_t ebx, uint32_t edx, uint32_t ecx,
	uint32_t eax, uint32_t eip, uint32_t cs, uint32_t eflags,
	uint32_t esp, uint32_t ss);
void game_drawvictory(int player);

// Arreglo de jugadores
player players[2];

#endif  /* !__GAME_H__ */
